# Site internet de Raoull

Ce dépôt de code contient le code PHP nécéssaire au fonctionnement du site internet de l'association Raoull. Il est basé sur la technologie Spip.

# Pré-requis

Pour lancer le serveur, il vous faut au préalable avoir installé :

- Le langage *PHP*
- Un serveur HTTP (*nginx* ou *Apache*)
- Une base de donnée (*MariaDB* ou *MySQL*)

Pour le téléchargement et la gestion des dépendances (librairie js), il vous
faut avoir installer *yarn* sur votre système.

# Installation en local

Récupérer le code avec Git :

```
git clone git@framagit.org:raoull/site-internet.git raoull-site
```

Puis lancer le script d'installation (cela installera Spip en version 3.2 + plugins):

```
make install
```

# Peupler la base de données


# Lancer le serveur

```
make run
```

# Configurer Spip

https:127.0.0.1/ecrire

# Activer les plugins

# Utilisation des librairies et outils (gulp et ses modules)

Lors du développement, quand vous souhaitez modifier les feuilles de styles ou
les js, n'oubliez pas de lancer gulp dans une console à part,
il s'occupera de compiler les fichiers à chaque modifications des
fichiers sources.

Pour savoir ce que fait exactement gulp, il suffit de lire
dans le fichier `squelettes/Gulpfile.js`
