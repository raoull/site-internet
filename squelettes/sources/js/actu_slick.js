

function initialisation_actu_slick(conteneur){

    if (conteneur === undefined)
        conteneur='body';

    $('.slider',conteneur).slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: false,
        arrows: true,
        dots: false,
        pauseOnHover: false,
        nextArrow: '<span class=" slick-next"><i class="fas fa-arrow-circle-right"></i></span>',
        prevArrow: '<span class=" slick-prev"><i class="fas fa-arrow-circle-left"></i></span>',
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 2
            }
        },
            {
                breakpoint: 300,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
}

