/**
 * Created by declic3000 on 14/06/16.
 */
/*!
 * Start Bootstrap - Grayscale Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery to collapse the navbar on scroll
function changeFondBarreNavigation() {
    position = $(window).scrollTop();
    if (position > 50) {
        $("#nav").addClass("under_the_top");
    } else {
        $("#nav").removeClass("under_the_top");
    }
}

$(window).scroll(changeFondBarreNavigation);

$('document').ready(function() {
    $('#menu_lang strong').each(function(){

        langue = $(this).text();
        langue = langue.toLowerCase();
        if (langue == 'en')
            langue ='gb';
        $(this).html('<span class="flag-icon flag-icon-'+langue+' flag-icon-squared"></span>');
    });
    $('#menu_lang a').each(function(){
        langue = $(this).attr('lang');
        langue = langue.toLowerCase();
        if (langue=='en')
            langue='gb';
        $(this).html('<span class="flag-icon flag-icon-'+langue+' flag-icon-squared"></span>');
    });

    $('#barre_de_navigation a').click(function(){
        $(".navbar-collapse").collapse('hide');
    });
    initialisation_actu_slick();
    initialisation_modal();
    initialisation_modal_form();
    changeFondBarreNavigation();
});


