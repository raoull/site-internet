

const config = {
    nodeDir: './node_modules/',
    bootstrapDir: './node_modules/bootstrap/',
    dir: './',
    publicDir: './../',
    debug:false
};


const fichiers = {
    'bootstrap': [
        config.dir + 'scss/bootstrap/scss/bootstrap.scss'
    ],
    'bootstrap2spip': [
        config.dir + 'scss/bootstrap2spip/css/boot.scss',
        config.dir + 'scss/bootstrap2spip/css/spip.list.scss',
        config.dir + 'scss/bootstrap2spip/css/spip.admin.scss',
        config.dir + 'scss/bootstrap2spip/css/spip.css'
    ],

    'sass': [
        config.nodeDir+'slick-carousel/slick/slick.scss',
        config.dir+'scss/slick-theme.scss',
        config.dir+'scss/ajustement.scss',
        config.dir+'scss/perso.scss'
    ],
    'styles': [
        //config.nodeDir+'flag-icon-css/css/flag-icon.min.css',
        //config.nodeDir + 'sidr/dist/stylesheets/jquery.sidr.light.css',
        //config.dir + '../fonts/fonts.css',
        config.dir + 'css/bootstrap.css',
        config.dir + 'css/bootstrap2spip.css',
        config.dir + 'css/styles_sass.css'
    ],
    'scripts': [
        //config.nodeDir+'jquery/dist/jquery.js',
        //config.nodeDir + 'sidr/dist/jquery.sidr.js',
        //config.nodeDir+'jquery-validation/dist/jquery.validate.js',
        config.dir+'js/all.fa-min.js',
        config.dir+'js/actu_slick.js',
        config.dir+'js/modal.js',
        config.dir+'js/perso.js'
    ],
    'scripts_bas_de_page': [
        config.bootstrapDir + 'dist/js/bootstrap.bundle.js',
        config.nodeDir+'slick-carousel/slick/slick.js',
        //config.dir+'js/lunr/mustache.js',
        //config.dir+'js/lunr/elasticlunr.min.js',
        //config.dir+'js/lunr/lunr.stremmer.support.min.js',
        //config.dir+'js/lunr/lunr.fr.min.js',
        //config.dir+'js/lunr/lunr.js'
    ],

};


const icones_fa = {
    fal: [],
    far: [],
    fas: ['rss','envelope','arrow-circle-right','arrow-circle-left','angle-double-down','download','map-marker','search','eye','tags','tag','chevron-right','bug','redo','exclamation-triangle','calendar','newspaper','bars','print','sign-out-alt','user','file-alt','file-audio','file-image','file-video'],
    fab: ['mastodon']
};


module.exports = [config,fichiers,icones_fa];